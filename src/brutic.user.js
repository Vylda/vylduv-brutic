// ==UserScript==
// @name         Vyldův Brutič
// @author       Vylda [Vilem Lipold]
// @copyright    2019-2021 Vilem Lipold
// @description  Geocaching coordinates checker brute force helper
// @version      2.0
// @homepage     http://www.gcgpx.cz/brutic/
// @namespace    http://www.gcgpx.cz/brutic/
// @icon         http://www.gcgpx.cz/brutic/mebadme.png
// @icon64       http://www.gcgpx.cz/brutic/mebadme64.png
// @match        https://www.geocaching.com/geocache/*
// @match        https://geocheck.org/geo_inputchkcoord.php*
// @updateURL    http://www.gcgpx.cz/brutic/brutic.meta.js
// @downloadURL  http://www.gcgpx.cz/brutic/brutic.user.js
// @grant        unsafeWindow
// ==/UserScript==

(function () {
  'use strict';

  try {
    var w = unsafeWindow || window;
    var lang = getLang() || 'en';
    var ls = w.localStorage;

    var evalTest;
    eval('ls.getItem(\'vylduv_brutic_info\')');
    eval('() => {}');
    eval('evalTest?.test');
  } catch (err) {
    console.error(err);
    showInfobox(
      _('oldBrowserBody'),
      'error',
      _('oldBroweserHeader')
    );
    return;
  }

  const ABOUT = {
    version: '2.0',
    news: {
      cs: `LISTING:
      - úprava chování tlačítek podle stavu Brutiče a ověřovátka
      - reload stránky po stisku tlačítka Otestovat další v případě vypršení recaptcha ověření
      - informace o vypršení recaptcha ověření

      GLOBÁLNĚ:
      - informační okno o zastaralém prohlížeči
      - okno Brutiče se otevře, pokud jsou nějaké souřadnice k ověření
      - Brutič pro Geocheck.org
      `,
      en: `LISTING:
      - adjustment of button behavior according to the state of the Brutic and the verifier
      - reload the page after pressing the Test next buttont in case of recaptcha verification expiration
      - recaptcha validation expiration information

      GLOBALLY:
      - information window about outdated browser
      - Brutic window will open if there are any coordinates to verify
      - Brutic for Geocheck.org
      `,
    },
  };

  function _(key) {
    if (!key) {
      return '';
    }

    var translations = {
      cs: {
        header: 'Vyldův Brutič',
        collapse: 'Sbalit Vyldův Brutič',
        expand: 'Rozbalit Vyldův Brutič',
        deleteConfirm: 'Doopravdy smazat všechny souřadnice?',
        counterTitle: 'Počet otestovaných souřadnic',
        timeTitle: 'Čas uplynulý od poslední kontroly',
        processCoordinates: 'Souřadnice ke zpracování',
        processingCoordinates: 'Aktuálně zpracovávané souřadnice',
        processedCoordinates: 'Zpracované souřadnice',
        again: 'Znovu',
        testNext: 'Otestovat další',
        deleteAllCoordinates: 'Smazat všechny souřadnice',
        notifyHeader: '10 minut uplynulo!',
        notifyBody: 'Můžeš obnovit stránku stisknutím tlačítka Otestovat další a znovu brutit!',
        recaptchaError: 'Ověření proti robotům nebylo úspěšné.',
        reload: 'Obnovuji stránku.',
        userCaptchaInfo: 'Vlož pět číslic a zmáčkni Enter!',
        oldBroweserHeader: 'Tvůj prohlížeč je starý',
        oldBrowserBody: 'Tvůj prohlížeč je zastaralý, pro provoz Vyldova Brutiče jej upgraduj na nejnovější verzi (nepoužívej Internet Explorer)!',
        appUpgrade: 'Vyldův Brutič v.',
        whatsNew: 'co je nového:',
        switchToOne: 'Pro fungování Vyldova Brutiče přepněte zobrazení na jedno pole.',
      },
      en: {
        header: 'Vylda\'s Brutic',
        collapse: 'Collapse Vylda\'s Brutic',
        expand: 'Expand Vylda\'s Brutic',
        deleteConfirm: 'Really delete all coordinates?',
        counterTitle: 'Count of tested coordinates',
        timeTitle: 'Time elapsed since last check',
        processCoordinates: 'Coordinates to process',
        processingCoordinates: 'Currently processed coordinates',
        processedCoordinates: 'Processed coordinates',
        again: 'Again',
        testNext: 'Test next',
        deleteAllCoordinates: 'Delete all coordinates',
        notifyHeader: '10 minutes elapsed!',
        notifyBody: 'You can reload page by click the Test next button and test another coordinates!',
        recaptchaError: 'Robots verification was not successful.',
        reload: 'Reloading.',
        userCaptchaInfo: 'Type five numbers and press Enter!',
        oldBroweserHeader: 'Your browser is old',
        oldBrowserBody: 'Your browser is outdated for operation of Vylda\'s Brutic, upgrade your browser to the latest version (do not use Internet Explorer)!',
        appUpgrade: 'Vylda\'s Brutic v.',
        whatsNew: 'what\'s new:',
        switchToOne: 'For Vylduv Brutic to work, switch to one field.',
      },
    }

    return translations[lang][key] || key;
  }

  function getLang() {
    var czLangs = ['sk', 'cs'];
    if (czLangs.indexOf(w.navigator.language) > -1) {
      return 'cs';
    }
    return 'en';
  }

  function showInfobox(text, type = "info", header = '') {
    if (!text) {
      return;
    }

    var modal = document.createElement('div');
    modal.style.position = 'fixed';
    modal.style.top = '0';
    modal.style.left = '0';
    modal.style.right = '0';
    modal.style.bottom = '0';
    modal.style.backgroundColor = 'black';
    modal.style.backgroundColor = 'rgba(0,0,0,.75)';
    modal.style.zIndex = '100000';
    modal.style.fontFamily = 'sans-serif';

    var wrapper = document.createElement('div');
    wrapper.style.maxWidth = '50vw';
    wrapper.style.maxHeight = '80vh';
    wrapper.style.position = 'absolute';
    wrapper.style.padding = '20px 60px 20px 20px';
    wrapper.style.boxSizing = 'border-box';
    wrapper.style.top = '50%';
    wrapper.style.left = '50%';
    wrapper.style.marginLeft = '-200px';
    wrapper.style.marginTop = '-50px';
    wrapper.style.borderRadius = '0.5em';
    wrapper.style.borderStyle = 'solid';
    wrapper.style.borderWidth = '2px';

    switch (type) {
      case 'info':
        wrapper.style.backgroundColor = '#fffed8';
        wrapper.style.color = '#686500';
        wrapper.style.borderColor = '#e3dd00';
        break;
      case 'error':
        wrapper.style.backgroundColor = '#ffeeee';
        wrapper.style.color = '#6c0606';
        wrapper.style.borderColor = '#6c0606';
        break;
      default:
        wrapper.style.backgroundColor = '#ffffff';
        wrapper.style.color = '#666666';
        wrapper.style.borderColor = '#666666';
    }

    if (header) {
      var h3 = document.createElement('h3');
      h3.textContent = header;
      h3.style.fontSize = '22px';
      h3.style.margin = '0 0 15px';
      wrapper.appendChild(h3);
    }

    var textWrapper = document.createElement('div');
    textWrapper.textContent = text;
    textWrapper.style.fontSize = '16px';
    textWrapper.style.whiteSpace = 'pre-line';

    var closeButton = document.createElement('button');
    closeButton.innerHTML = '&times;';
    closeButton.addEventListener('click', function () {
      var parent = modal.parentNode;
      if (parent) {
        parent.removeChild(modal);
      }
    });
    closeButton.style.position = 'absolute';
    closeButton.style.top = '10px';
    closeButton.style.right = '10px';
    closeButton.style.width = '30px';
    closeButton.style.height = '30px';
    closeButton.style.textAlign = 'center';
    closeButton.style.padding = '0';
    closeButton.style.margin = '0';
    closeButton.style.borderRadius = '50%';
    closeButton.style.border = '1px solid #666';
    closeButton.style.color = '#666';
    closeButton.style.backgroundColor = '#eee';
    closeButton.style.cursor = 'pointer';
    closeButton.style.fontSize = '20px';

    wrapper.appendChild(textWrapper);
    wrapper.appendChild(closeButton);

    modal.appendChild(wrapper)
    document.body.appendChild(modal);

    const bb = wrapper.getBoundingClientRect();
    console.log(bb);
    wrapper.style.marginLeft = `${-(bb.width / 2)}px`;
    wrapper.style.marginTop = `${-(bb.height / 2)}px`;

  }

  const ss = w.sessionStorage;
  const DOM = {};

  const domainTest = /geocaching\.com$/i;
  const PAGE_GEOCACHING = 'geocaching';
  const PAGE_GEOCHECK = 'geocheck';

  const PAGE = domainTest.test(window.location.hostname) ? PAGE_GEOCACHING : PAGE_GEOCHECK;

  const STORAGE_INFO_NAME = 'vylduv_brutic_info';
  const STORAGE_ITEM_NAME = 'vylduv_brutic_data';
  const STORAGE_ITEM_NAME_CHECK = 'vylduv_brutic_data_geocheck';
  const STORAGE_NAME = PAGE === PAGE_GEOCACHING ? STORAGE_ITEM_NAME : STORAGE_ITEM_NAME_CHECK;

  const state = {
    untested: [],
    current: '',
    tested: [],
    date: null,
  };
  let timer = null;
  let notified = false;


  const computeCount = () => {
    const tested = state.tested.length + (state.current ? 1 : 0)
    const total = state.untested.length + tested;
    DOM.counter.textContent = total ? `${tested}/${total}` : '';
  };

  const saveState = () => {
    const jsonState = JSON.stringify(state);
    ss.setItem(STORAGE_NAME, jsonState);
  };

  const getElapsedTime = () => {
    if (!state.date) {
      state.date = new Date();
    }
    const now = new Date();
    const { date } = state;
    const ms = now - date;
    return Math.floor(ms / 1000);
  };

  const isLimited = () => {
    if (getElapsedTime() > 600 || !DOM.solutionInfo) {
      return false;
    }

    let regex;
    if (PAGE === PAGE_GEOCACHING) {
      regex = /10/;
    } else {
      regex = /10[a-ž ]+10/i;
    }

    const limited = regex.test(DOM.solutionInfo.textContent); // Test for 10 minutes in solution info
    return limited;
  };

  const captched = () => {
    DOM.recaptchaError.textContent = '';
    if (PAGE === PAGE_GEOCHECK) {
      return true;
    }
    const captcha = w.grecaptcha;
    if (captcha && !captcha.getResponse()) {
      DOM.recaptchaError.textContent = _('recaptchaError');
      return false;
    }
    return true;
  };

  const checkTestPossible = () => {
    if (!state.untested.length) {
      DOM.checkButton.disabled = true;
      return false;
    }
    const result = isLimited() || !captched();
    DOM.checkButton.disabled = result;
    return !result;
  };

  const checkRepeatPossible = () => {
    if (!state.current) {
      DOM.repeatButton.disabled = true;
      return false;
    }
    const result = isLimited() || !captched();
    DOM.repeatButton.disabled = result;
    return !result;
  };

  const insertStyle = () => {
    const style = `
          @import url('https://fonts.googleapis.com/css2?family=Baloo+2:wght@400;700&display=swap');
          .brutic-user-captcha-info {
            font-size: 85%;
            font-family: 'Baloo 2', sans-serif;
            font-weight: 400;
            padding-top: 0.5em;
            padding-left: 0.5em;
            color: #595959;
          }
          #vylduv-brutic {
            position: fixed;
            bottom: 50px;
            right: 50px;
            z-index: 99999;
            width: 250px;
            background-color: rgba(255,255,255,0.75);
            box-sizing: content-box;
            padding: 10px;
            border: 1px solid #666;
            box-shadow: 4px 4px 8px rgba(127,127,127,0.25);
            font-family: 'Baloo 2', sans-serif;
            font-weight: 400;
          }
          #vylduv-brutic.hidden {
            opacity: 0.5;
          }
         #vylduv-brutic .vylduv-brutic-head-wrapper {
           margin-bottom: 10px;
           display: flex;
           justify-content: space-between;
           align-items: flex-start;
         }
         #vylduv-brutic.hidden .vylduv-brutic-head-wrapper {
           margin-bottom: 0;
         }
         #vylduv-brutic.hidden .vylduv-brutic-head-wrapper .vylduv-brutic-counter {
           display: none;
         }
         #vylduv-brutic h3 {
           margin: 0;
         }
         #vylduv-brutic .vylduv-brutic-head-wrapper .vylduv-brutic-small-info {
           display: inline-block;
           margin-left: 10px;
           font-size: 60%;
           font-weight: 400;
           position: relative;
           top: -0.3rem;
         }
         #vylduv-brutic .vylduv-brutic-head-wrapper .vylduv-brutic-timer:empty {
           display: none;
         }
         #vylduv-brutic .vylduv-brutic-head-wrapper .brutic-minmax-button {
           background-color: transparent;
           border: 1px dotted #ccc;
           border-bottom: 2px solid #666;
           width: 1rem;
           height: 1rem;
           display: block;
           box-sizing: border-box;
           padding: 0;
           cursor: pointer;
         }
         #vylduv-brutic .vylduv-brutic-head-wrapper .brutic-minmax-button.hidden {
           border: 2px solid #666;
         }
         #vylduv-brutic .vylduv-brutic-wrapper.hidden {
           display: none;
         }
         #vylduv-brutic .brutic-input {
           display: block;
           width: 100%;
           height: 10rem;
           font-family: Consolas, Courier New, Courier, Lucida Console, monospace;
           padding: 5px;
           box-sizing: border-box;
           font-size: 75%;
         }
         #vylduv-brutic .brutic-current-coords {
           min-height: 1.5rem;
           width: 100%;
           background-color: rgb(233, 233, 255);
           border: 1px solid rgb(3, 3, 130);
           margin: 5px 0;
           padding: 5px;
           box-sizing: border-box;
         }
         #vylduv-brutic .brutic-current-coords:empty {
          display: none;
         }
         #vylduv-brutic .brutic-recaptcha-error {
           background-color: #fff5f5;
           border: 1px solid #9f0000;
           width: 100%;
           box-sizing: border-box;
           margin-top: 20px;
           font-weight: 800;
           padding: 5px 15px;
           border-radius: 0.25rem;
         }
         #vylduv-brutic .brutic-recaptcha-error:empty {
           display: none;
         }
         #vylduv-brutic .brutic-tested-coords {
           min-height: 2rem;
           background-color: rgb(244, 244, 244);
           border: 1px solid rgb(100,100,100);
           color: rgb(100,100,100);
           padding: 5px;
           width: 100%;
           box-sizing: border-box;
           margin: 0 0 5px;
           max-height: 10rem;
           overflow-y: auto;
           white-space: pre;
           font-size: 75%;
           font-family: Consolas, Courier New, Courier, Lucida Console, monospace;
         }
         #vylduv-brutic .brutic-tested-coords:empty {
           display: none;
         }
         #vylduv-brutic .brutic-button-container {
           display: flex;
           justify-content: space-between;
           margin-top: 20px;
           flex-wrap: wrap;
           gap: 15px;
         }
         #vylduv-brutic .brutic-button-container button {
           cursor: pointer;
           font-weight: 800;
           padding: 5px 15px;
           border-radius: 0.25rem;
         }
         #vylduv-brutic .brutic-repeat-button {
           background-color: #ffd993;
           border: 1px solid #9d6600;
         }
         #vylduv-brutic .brutic-check-button {
           background-color: #deffd9;
           border: 1px solid #204f0b;
         }
         #vylduv-brutic .brutic-delete-button {
           background-color: #fff5f5;
           border: 1px solid #9f0000;
           width: 100%;
         }
         #vylduv-brutic .brutic-button-container button:disabled {
           background-color: #eee;
           color: #aaa;
           border-color: #aaa;
           cursor: not-allowed;
         }
         #vylduv-brutic .brutic-button-container button:active:not(:disabled) {
           box-shadow: inset 2px 2px rgba(0,0,0,0.35);
         }
        `;
    const styleElement = document.createElement('style');
    styleElement.innerHTML = style;

    document.head.appendChild(styleElement);
  };

  const computeTime = () => {
    const totalSecs = getElapsedTime();
    if (totalSecs > 600) {
      DOM.repeatButton.disabled = !state.current;
      DOM.checkButton.disabled = !state.untested.length;
      const notifyEvent = new Event('notifyMe');
      DOM.timerWrapper.dispatchEvent(notifyEvent);
    }
    const secs = totalSecs % 60;
    const mins = Math.floor(totalSecs / 60);
    DOM.timerWrapper.textContent = `${mins.toString(10).padStart(2, 0)}:${secs.toString(10).padStart(2, 0)}`;
  };

  const saveTime = () => {
    state.date = new Date();
    saveState();
  };

  const startTimer = () => {
    if (timer) {
      clearInterval(timer);
    }
    computeTime();
    timer = setInterval(() => {
      computeTime();
    }, 500);
  };

  const showTested = () => {
    DOM.tested.textContent = state.tested.join('\n');
    DOM.tested.scrollTop = DOM.tested.scrollHeight;
  };

  const showCurrent = () => {
    DOM.current.textContent = state.current;
    DOM.repeatButton.disabled = !state.current;
  };

  const checkButtonState = () => {
    DOM.checkButton.disabled = !state.untested.length;
  };

  const toggleMinMax = (evt) => {
    const { target } = evt;
    const isHidden = target.classList.contains('hidden');
    target.classList[isHidden ? 'remove' : 'add']('hidden');
    target.title = isHidden ? _('collapse') : _('expand');
    target.classList[isHidden ? 'remove' : 'add']('hidden');
    DOM.wrapper.classList[isHidden ? 'remove' : 'add']('hidden');
    DOM.container.classList[isHidden ? 'remove' : 'add']('hidden');
    if (isHidden) {
      DOM.checker.scrollIntoView(false);
      if (
        "Notification" in w
        && (Notification.permission !== 'denied' || Notification.permission === "default")
      ) {
        Notification.requestPermission();
      }
    }
  };

  const handleInput = (evt) => {
    const { target } = evt;
    const { value } = target;
    const untestedArray = value.split('\n');
    state.untested = untestedArray.reduce((acc, val) => {
      const value = val.trim();
      if (value) {
        acc.push(value);
      }
      return acc;
    }, []);
    state.tested = [];
    state.current = '';
    showTested();
    showCurrent();
    checkButtonState();

    saveState();
    target.scrollTop = 0;
  };

  const observeGeocachingChecker = () => {
    if (!DOM.solutionInfo || !w.MutationObserver) {
      return;
    }
    const observer = new MutationObserver(() => {
      observer.disconnect();
      checkTestPossible();
      checkRepeatPossible();
    });

    observer.observe(DOM.solutionInfo, {
      characterData: false,
      attributes: false,
      childList: true,
      subtree: false,
    });
  };

  const handleRepeat = () => {
    DOM.coordsInput.value = state.current;
    if (DOM.solutionInfo) {
      if (isLimited() || !captched()) {
        return;
      }
    }
    computeCount();
    saveTime();
    startTimer();
    notified = false;
    if (PAGE === PAGE_GEOCACHING) {
      w.CheckCoords();
      observeGeocachingChecker();
    } else if (DOM.userCaptcha) { // GEOCHECK
      DOM.userCaptcha.focus();
    }
  };

  const handleCheck = () => {
    if (!DOM.coordsInput || !state.untested.length) {
      checkButtonState();
      return;
    }

    if (DOM.solutionInfo) {
      if (isLimited() || !captched()) {
        if (!captched()) { // LISTING
          const recaptchaWrapper = document.querySelector('.g-recaptcha[data-sitekey]');
          if (recaptchaWrapper && recaptchaWrapper.style.display === 'none') {
            DOM.recaptchaError.textContent = `${_('recaptchaError')} ${_('reload')}`;
            setInterval(() => {
              w.location.reload();
            }, 1000);
          }
        }
        checkRepeatPossible();
        checkTestPossible();
        return;
      }
    }

    if (state.current) {
      state.tested.push(state.current);
      showTested();
    }

    state.current = state.untested.shift();
    DOM.repeatButton.disabled = !!state.current;
    DOM.input.value = state.untested.join('\n');
    DOM.input.scrollTop = 0;
    showCurrent();
    checkButtonState();
    if (PAGE === PAGE_GEOCHECK) {
      DOM.repeatButton.disabled = true;
      DOM.checkButton.disabled = true;
    }
    saveState();
    handleRepeat();
  };

  const handleDelete = () => {
    if (!confirm(_('deleteConfirm'))) {
      return;
    }
    state.untested = [];
    state.current = '';
    state.tested = [];
    state.date = null;
    saveState();
    showTested();
    showCurrent();
    if (timer) {
      clearInterval(timer);
    }
    DOM.input.value = '';
    DOM.timerWrapper.textContent = '';
    checkButtonState();
  }

  const notify = () => {
    if ("Notification" in w && !notified) {
      if (Notification.permission === "granted") {
        notified = true;
        new Notification(_('notifyHeader'), {
          lang: lang,
          body: _('notifyBody'),
        });
      }
    }
  };

  const build = () => {
    insertStyle();

    const container = document.createElement('div');
    container.id = 'vylduv-brutic';
    if (!state.untested.length && !state.current) {
      container.classList.add('hidden');
    }
    const headWrapper = document.createElement('div');
    headWrapper.classList.add('vylduv-brutic-head-wrapper');

    const header = document.createElement('h3');
    header.textContent = _('header');

    const counter = document.createElement('div');
    counter.classList.add('vylduv-brutic-counter');
    counter.classList.add('vylduv-brutic-small-info');
    counter.title = _('counterTitle');

    const timerWrapper = document.createElement('div');
    timerWrapper.classList.add('vylduv-brutic-timer');
    timerWrapper.classList.add('vylduv-brutic-small-info');
    timerWrapper.title = _('timeTitle');
    timerWrapper.addEventListener('notifyMe', notify);

    const minMaxButton = document.createElement('button');
    minMaxButton.classList.add('brutic-minmax-button');
    if (!state.untested.length && !state.current) {
      minMaxButton.classList.add('hidden');
    }
    minMaxButton.addEventListener('click', toggleMinMax);
    minMaxButton.title = _('expand');

    const wrapper = document.createElement('div');
    wrapper.classList.add('vylduv-brutic-wrapper');
    if (!state.untested.length && !state.current) {
      wrapper.classList.add('hidden');
    }

    const inputField = document.createElement('textarea');
    inputField.classList.add('brutic-input');
    inputField.addEventListener('input', handleInput);
    inputField.value = state.untested.join('\n');
    inputField.title = _('processCoordinates');

    const currentCoords = document.createElement('div');
    currentCoords.classList.add('brutic-current-coords');
    currentCoords.textContent = state.current;
    currentCoords.title = _('processingCoordinates');

    const testedCoords = document.createElement('div');
    testedCoords.classList.add('brutic-tested-coords');
    testedCoords.textContent = state.tested.join('\n');
    testedCoords.title = _('processedCoordinates');

    const buttonConstainer = document.createElement('div');
    buttonConstainer.classList.add('brutic-button-container');

    const repeatButton = document.createElement('button');
    repeatButton.classList.add('brutic-repeat-button');
    repeatButton.type = 'button';
    repeatButton.textContent = _('again');
    repeatButton.addEventListener('click', handleRepeat);

    const checkButton = document.createElement('button');
    checkButton.classList.add('brutic-check-button');
    checkButton.type = 'button';
    checkButton.textContent = _('testNext');
    checkButton.addEventListener('click', handleCheck);

    const deleteButton = document.createElement('button');
    deleteButton.classList.add('brutic-delete-button');
    deleteButton.type = 'button';
    deleteButton.textContent = _('deleteAllCoordinates');
    deleteButton.addEventListener('click', handleDelete);

    const recaptchaError = document.createElement('div');
    recaptchaError.classList.add('brutic-recaptcha-error');

    header.appendChild(counter);
    header.appendChild(timerWrapper);
    headWrapper.appendChild(header);
    headWrapper.appendChild(minMaxButton);

    wrapper.appendChild(inputField);
    wrapper.appendChild(currentCoords);
    wrapper.appendChild(testedCoords);

    wrapper.appendChild(recaptchaError);

    buttonConstainer.appendChild(repeatButton);
    buttonConstainer.appendChild(checkButton);
    buttonConstainer.appendChild(deleteButton);
    wrapper.appendChild(buttonConstainer);

    container.appendChild(headWrapper);
    container.appendChild(wrapper);

    document.body.appendChild(container);

    DOM.container = container;
    DOM.counter = counter;
    DOM.timerWrapper = timerWrapper;
    DOM.input = inputField;
    DOM.current = currentCoords;
    DOM.tested = testedCoords;
    DOM.recaptchaError = recaptchaError;
    DOM.checkButton = checkButton;
    DOM.repeatButton = repeatButton;
    DOM.wrapper = wrapper;
    if (PAGE === PAGE_GEOCACHING) {
      DOM.coordsInput = document.querySelector('input.solution-input');
      DOM.solutionInfo = document.querySelector('#lblSolutionResponse');
    } else {
      DOM.coordsInput = DOM.checker;
      DOM.solutionInfo = document.querySelector('tr td.attemptred');
      DOM.userCaptcha = document.querySelector('input[name=usercaptcha]');
      if (DOM.userCaptcha) {
        const parent = DOM.userCaptcha.parentNode;
        const info = document.createElement('div');
        info.classList.add('brutic-user-captcha-info');
        info.textContent = _('userCaptchaInfo');
        parent.appendChild(info);
      }
    }
  };

  const init = () => {
    if (PAGE === PAGE_GEOCACHING) {
      DOM.checker = document.querySelector('.CoordChecker');
      if (!DOM.checker || !w.CheckCoords) {
        return;
      }
    } else if (PAGE === PAGE_GEOCHECK) {
      DOM.checker = document.querySelector('input[name=coordOneField]');
      if (!DOM.checker) {
        const switchButton = document.querySelector('input[name=switchOneField]');
        if (switchButton) {
          const row = switchButton.parentNode.parentNode;
          const table = row.parentNode;
          const infoRow = document.createElement('tr');
          const infoCell = document.createElement('td');
          infoCell.colSpan = 2;
          infoCell.textContent = _('switchToOne');
          infoCell.style.fontStyle = 'italic';
          infoCell.style.fontWeight = 'bold';
          infoCell.style.fontSize = '80%';
          infoCell.style.color = '#999';
          infoCell.style.textAlign = 'center';
          infoCell.style.padding = '10px 0 5px';

          infoRow.appendChild(infoCell);
          table.insertBefore(infoRow, row.nextSibling);
        }
        return;
      }
    } else {
      return;
    }

    const savedState = JSON.parse(ss.getItem(STORAGE_NAME));
    if (savedState) {
      const { untested, current, tested, date } = savedState;
      state.untested = untested;
      state.current = current;
      state.tested = tested;
      state.date = date ? new Date(date) : null;
    }
    build();
    computeCount();

    if (!state.untested.length) {
      DOM.checkButton.disabled = true;
    }
    if (!state.current) {
      DOM.repeatButton.disabled = true;
    }

    if (state.date) {
      startTimer();
    }

    if (PAGE === PAGE_GEOCHECK) {
      checkTestPossible();
      checkRepeatPossible();
    }
  };

  const showAndSaveNewVersion = () => {
    const variables = [
      ABOUT.news[lang],
      'info',
      `${_('appUpgrade')} ${ABOUT.version} – ${_('whatsNew')}`,
    ];
    showInfobox(...variables);

    const dataToSave = {
      version: ABOUT.version,
    };
    ls.setItem(STORAGE_INFO_NAME, JSON.stringify(dataToSave));
  }

  const showNewVersionInfo = () => {
    const infoDataLS = ls.getItem(STORAGE_INFO_NAME);

    if (infoDataLS) {
      const infoData = JSON.parse(infoDataLS);
      const savedVersion = infoData.version;

      if (!savedVersion || (savedVersion && ABOUT.version > savedVersion)) {
        showAndSaveNewVersion();
      }
    } else {
      showAndSaveNewVersion();
    }
  };

  showNewVersionInfo();

  w.addEventListener('load', () => {
    init();
  });
})();
