// ==UserScript==
// @name         Vyldův Brutič
// @author       Vylda [Vilem Lipold]
// @copyright    2019-2021 Vilem Lipold
// @description  Geocaching coordinates checker brute force helper
// @version      2.0
// @homepage     http://www.gcgpx.cz/brutic/
// @namespace    http://www.gcgpx.cz/brutic/
// @icon         http://www.gcgpx.cz/brutic/mebadme.png
// @icon64       http://www.gcgpx.cz/brutic/mebadme64.png
// @match        https://www.geocaching.com/geocache/*
// @match        https://geocheck.org/geo_inputchkcoord.php*
// @updateURL    http://www.gcgpx.cz/brutic/brutic.meta.js
// @downloadURL  http://www.gcgpx.cz/brutic/brutic.user.js
// @grant        unsafeWindow
// ==/UserScript==
