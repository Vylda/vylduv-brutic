# Jak spustit Vyldův Brutič

Občas je třeba vybrutit souřadnice na kešce. Pokud má listing ověřovač od Goundspeaku, Vyldův Brutič pomůže.

Brutič funguje i na doméně **Geocheck.org**.

Po uplynutí 10 minut dá Vyldův Brutič pomocí oznámení prohlížeče vědět, že lze znovu brutit.

## Instalace

1.  Je potřeba mít v prohlížeči nainstalovaný nějaký spouštěč skriptů.
    *  pro Firefox: [Tamper Monkey](https://addons.mozilla.org/cs/firefox/addon/tampermonkey/), [Grease Monkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/)
    *  pro Chrome: [Tamper Monkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)

2.  Je třeba nainstalovat [Vyldův Brutič](http://gcgpx.cz/brutic/brutic.user.js).
    *  Pokud je nainstalovaný Monkey (viz odkazy výše), dojde při kliknutí na odkaz [Vyldův Brutič](http://gcgpx.cz/brutic/brutic.user.js) k automatickému spuštění instalace.

## Použití
1. Po úspěšné instalaci se vpravo dole v listingu keše, která má nativní ověřovač od Groundspeaku, objeví sbalený Vyldův Brutič.
2. Po rozbalení (kliknutím na ikonku v pravém horním rohu Brutiče) můžeš do horního políčka nasypat souřadnice. Každá souřadnice musí být na novém řádku. Zároveň dojde k zarolování stránky tak, aby byl vidět ověřovač. Při prvním rozbalení Vyldova Brutiče tě prohlížeč požádá o povolení k zobrazování oznámení. Pokud chceš být informován, že uplynulo 10 minut, dej k tomu souhlas.
3. Po vložení souřadnic do horního políčka můžeš otestovat první souřadnici v seznamu kliknutím na tlačítko **Otestovat další**. Samozřejmě je třeba ověřit, že nejsi robot.
4. Na doméně **Geocheck.org** to funguje trochu jinak. Po stisknutí tlačítka **Otestovat další** se vloží do políčka pro kontrolu souřadnice a textový kurzor se přesune do pole pro zadání pěti čísel. Po jejich zapsání stačí stisknout klávesu Enter (myš tedy může zůstat nad tlačítkem otestovat další) a tím dojde k ověření.
5. Kliknutím na tlačítko **Znovu** provedeš novou kontrolu souřadnice, která je v ověřovači (je zobrazena i v modrém políčku Brutiče). V šedivém poli se zobrazují již otestované souřadnice. Kliknutím na tlačítko **Otestovat další** otestuješ další souřadnici ze seznamu.
6. Po kliknutí na tlačítko **Smazat všechny souřadnice** smažeš všechna data ve Vyldově Brutiči. Data jsou uložena zvlášť pro Geocaching.com a Geocheck.org.

## Upozornění

1. Vyldův Brutič **neobchází** ani limit deseti pokusů za deset minut, ani ověřovač Captcha (že nejsi robot).
2. Vyldův Brutič si **pamatuje** zadané souřadnice (zvlášť pro každou doménu), takže můžeš stránku obnovit, ale nesmíš ji zavřít. Při zavření stránky (vypnutí prohlížeče či jeho záložky) se data z Vyldova Brutiče smažou.
3. Na stránce **geocheck.org** je třeba přepnout **z šesti polí na jedno pole**!

## License
[MIT](http://www.opensource.org/licenses/mit-license.php)
